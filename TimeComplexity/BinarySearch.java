package TimeComplexity;

import java.util.Scanner;

/**
 * Created by Amin Khaki Moghadam on 30/10/2016.
 * Time Complexity Project - O(LogN)
 */
class BinarySearch extends ArrayMaker{
    private boolean search(int key){
        long startTime = System.nanoTime();
        int first=0 , last=this.i-1;
        boolean assist = false; //If the number not founded, assist will remain false - Now I can calculate time easier.
        while (last >= first){
            int middle = (first+last)/2;
            if(this.num[middle] == key){
                assist = true;
                break;
            }
            else if (this.num[middle] < key){
                first = middle+1;
            }
            else if(this.num[middle] > key){
                last = middle-1;
            }
        }
        long endTime = System.nanoTime();
        System.out.println("This took " + (endTime-startTime) + " Nanoseconds");
        return assist;
    }
    static void logN(){
        BinarySearch temp = new BinarySearch();
        temp.input();
        temp.sort();
        System.out.println("Which number?");
        Scanner in = new Scanner(System.in);
        int reqNum = in.nextInt();
        if (temp.search(reqNum))
            System.out.println("Found");
        else
            System.out.println("Not Found");
    }
}
