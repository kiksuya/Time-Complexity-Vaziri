package TimeComplexity;

import java.util.Scanner;
/**
 * Created by Amin Khaki Moghadam on 10/11/2016.
 * Time Complexity Project - O(N)
 */
class LinearSearch extends ArrayMaker{
    private void lSearch(int search){
        int c;
        long startTime = System.currentTimeMillis();
        long nanoStart = System.nanoTime();
        for (c = 0; c < i; c++)
        {
            if (num[c] == search)
            {
                System.out.println(search + " Founded at location " + (c + 1) + ".");
                break;
            }
        }
        if (c == i)
            System.out.println(search + " Not founded in array.");
        long endTime = System.currentTimeMillis();
        long nanoEnd = System.nanoTime();
        System.out.println("\nThis took " + (endTime-startTime) + " Milliseconds");
        System.out.println("\nThis took " + (nanoEnd-nanoStart) + " Nanoseconds");
    }
    static void N(){
        LinearSearch temp = new LinearSearch();
        temp.input();
        int search;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter value to find: ");
        temp.lSearch(search = in.nextInt());
    }
}
