package TimeComplexity;

import java.util.Scanner;

/**
 * Created by Amin Khaki Moghadam on 16/10/2016.
 * Main Class
 */

public class Gui{
    public static void main(String args[]){
        System.out.println("Choice?");
        Scanner in = new Scanner(System.in);
        int user = in.nextInt();
        switch (user){
            case 1:
                BinarySearch.logN();
                break;
            case 2:
                BubbleSort.N82();
                break;
            case 3:
                LinearSearch.N();
                break;
            case 4:
                TowersOfHanoi.h2n();
                break;
            case 5:
                QuickSort.nLogN();
                break;
        }
    }
}